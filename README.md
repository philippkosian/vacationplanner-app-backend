### What? ###

Secure PHP-Backend (API) for the VacationPlanner App

**Prod** contains a single file which is used inside the app folder on the server. It already links to the rest of the API on the server.

**Dev** contains the development environment with the necessary API files inside the 'pathtodefault' folder. Apparently there are still some files missing from the server such as js/config.json (probably where the usergroups are defined).