<?php 


    // /*
        // Backend Service for VacationPlannerApp:
        // - Receives commands from the app
        // - Retrieves entries from the MySQL database and sends them back to the app in JSON

        // JSON structure:
        // {
            // "login": {
                // email: ...,
                // password: ...
            // },
            // action: (login, get-users, get-dates, write-date, delete-date),
            // (user: (ID),
            // date: ...,
            // span: ...)

        // }

        // {
        // "login": {
        // "email": "oskar2@publicispixelpark.de",
        // "password": "Erb8&Eco!9"
        // },
        // "action": "login"
        // }
    // */

    require_once "./pathtodefault/SecureInput/SecureInput.class.php";
    require_once "./pathtodefault/password_compat-1.0.4/lib/password.php";


    /* TODO FOR MERGE WITH WEB APP:
        - Change Paths
    */

    // Path to ressource and API files
    $path = './pathtodefault/';
    $config_path = './pathtodefault/js/config.json';

    require $path.'datenbank-verbindung.php';
    require $path.'Urlaub.class.php';
    require $path.'User.class.php';

    // Creates instances of the database getter/setter classes
    $UserClass = new User($db_link);
    $UrlaubClass = new Urlaub($db_link);
    
    $usertype_admin = 1;
    $usertype_standard = 0;




    // Retrieve json data
    // ====================

    $data = json_decode(file_get_contents('php://input'));
    // echo json_encode($data);




    // Check login and match action with function
    // ===========================================

    
    $result = array(false);     // Default result

    
    if(isset($data) && !empty($data)) {     // If data is available
        
        if(isset($data->login) && isset($data->action)) {   // If json data has "login" and "action"
            
            $loginResult = login($data->login);     // Validate and retrieve user information
            

            if( $loginResult ) {        // Login was valid


                // Check action and call corresponding function
                // =============================================

                if( $data->action == "login" ) {
                    $result = $loginResult;     // is automatically true because login has already been checked
                }

                else if( $data->action == "get-users" ) {
                    if( isset($data->usergroup)) {
                        $result = getUsers($data->usergroup);
                    } else {
                        $result = getUsers(null);
                    }
                }

                else if( $data->action == "get-dates" ) {

                    if( isset( $data->yearmonth ) ) {       // no option yet to retrieve all dates per year or overall
                        $result = getDatesPerMonth( $data->yearmonth );
                    } else {
                        $result = array(false);
                    }

                }

                else if( $data->action == "write-date" ) {

                    if( isset($data->user_id) 
                        && isset($data->date) ) {
                        // if usertype 1, then user has to be the same user whose vacation is being edited
                        if( $loginResult['type'] == $usertype_standard && $loginResult['id'] == $data->user_id) {
                            $result = array( writeDate( $data->user_id, $data->date, $loginResult['type'] ) );
                        // else if usertype 2 (admin), allow editing any user
                        } else if( $loginResult['type'] == $usertype_admin ) {
                            $result = array( writeDate( $data->user_id, $data->date, $loginResult['type'] ) );
                        } else {
                            $result = array(false);
                        }
                    } else {
                        $result = array(false);
                    }

                }

                else if( $data->action == "delete-date" ) {

                    if( isset($data->user_id) 
                        && isset($data->date) ) {
                        $result = array(deleteDate( $data->user_id, $data->date, $loginResult['type']));
                    } else {
                        $result = array(false);
                    }

                }

                else if( $data->action == "get-usergroups") {
                    $result = getUserGroups();
                }
            }
        }
    }


    // Output:
    //header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($result);
    


    // validate login info and return user information
    function login($login) {

        global $UserClass;

        $result = false;

        if(isset($login->email) && isset($login->password)) {
            
            $dbUser = $UserClass->getUserByEmail($login->email);

            $usertype = $UserClass->getUserType($dbUser['user_id']);

            $SecureInput = new SecureInput(false);
            $passwordSecure = $SecureInput->filter($login->password, SecureInput::SI_TYPE_PLAINEXT_COMMENT);
            
            // user exists and password is correct
            if($dbUser && password_verify($passwordSecure, $dbUser['password'])) {
                $result = array(
                    "id"=>$dbUser['user_id'],
                    "name"=>$dbUser['name'],
                    "email"=>$dbUser['email'],
                    "type"=>$usertype['usertype']
                );
            }

        }

        return $result;
    }


    function getUsers($usergroup) {
        
        global $UserClass;
        $list = $UserClass->getList($usergroup);
        $listSimple = array();
        foreach($list as $user) {
            
            array_push($listSimple, array(  "id"=>$user['user_id'],
                                            "name"=>$user['name']
                                    )
            );
            
        }
        return $listSimple;
    }

    function getUserGroups() {
        global $config_path;
        $config_json = file_get_contents($config_path);     // Get json contents
        $config_json_arr = json_decode($config_json, true);

        $usergroups = array();
        foreach(array_keys($config_json_arr['Groups']) as $key) {   // Create better array and return
            array_push( $usergroups, array(  $key, $config_json_arr['Groups'][$key]['dropdownText'] ) );
        }
        return $usergroups;
    }

    function getDatesPerMonth($yearmonth) {
        global $UrlaubClass;
        return $UrlaubClass->getListForMonth($yearmonth);
    }


    // TODO: restrict to usertype!!!
    function writeDate($user_id, $date) {
        global $UrlaubClass;
        return $UrlaubClass->createHolliday($user_id, $date);
    }

    function deleteDate($user_id, $date) {
        global $UrlaubClass;
        return $UrlaubClass->deleteHolliday($user_id, $date);
    }



    function timestamp() {
        $timeStampData = microtime();
        list($msec, $sec) = explode(' ', $timeStampData);
        $msec = round($msec * 1000);

        return $sec .' - '. $msec;
    }

?>