<?php

///////////////////////////////////////////////////////////////////////////////
/**
 * Unittest for SecureInput class
 * 
 * @author Tobias Bulla
 * @copyright 2010 P//MOD Germany
 * @package SecureInput
 * @version 1.0
 */

///////////////////////////////////////////////////////////////////////////////
/**
 * Include the PHPUnit core framework.
 * Use pear to install phpunit on your local php instance
 * CMD: pear channel-discover pear.phpunit.de
 * CMD: pear install phpunit/PHPUnit
 * Set phpunit to your binary execution path and init this test class with
 * CMD: /unittest/phpunit PageTest.php
 */

require_once('PHPUnit/Framework.php');

include_once("SecureInput.class.php");

///////////////////////////////////////////////////////////////////////////////
/**
 * 
 * @package SecureInput
 */

class UnitTest extends PHPUnit_Framework_TestCase 
{
    public function testSecurity()
    {
        $_GET["test1"] = "One";
        $_POST["test2"] = "Two";
        $_COOKIE["test3"] = "Three";
        
        $si = new SecureInput(true, true);
        
        // Test removal of normal arrays
        $this->assertSame(count($_GET), 0);
        $this->assertSame(count($_POST), 0);
        $this->assertSame(count($_REQUEST), 0);
        $this->assertSame(count($_COOKIE), 0);
        
        // Test different sources
        $this->assertSame("fail", $si->get("test1", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_POST));      
        $this->assertSame("fail", $si->get("test2", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_GET)); 
        $this->assertSame("fail", $si->get("test3", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_GET));    
        
        $this->assertSame("One", $si->get("test1", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_GET));      
        $this->assertSame("Two", $si->get("test2", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_POST)); 
        $this->assertSame("Three", $si->get("test3", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_COOKIE));          
    }	

    public function testMultiInstance()
    {
        $_GET["test1"] = "One";
        $_POST["test2"] = "Two";
        $_COOKIE["test3"] = "Three";
    	
        $s1 = new SecureInput(true, true);
        $s2 = new SecureInput();

        $this->assertSame("One",   $s1->get("test1", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_GET));      
        $this->assertSame("Two",   $s1->get("test2", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_POST)); 
        $this->assertSame("Three", $s1->get("test3", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_COOKIE));          
        
        $this->assertSame("One",   $s2->get("test1", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_GET));      
        $this->assertSame("Two",   $s2->get("test2", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_POST)); 
        $this->assertSame("Three", $s2->get("test3", SecureInput::SI_TYPE_PLAIN, "fail", true, false, SecureInput::SI_SOURCE_COOKIE));          
    }
	
    public function testGetBool()
    {
        $_GET["test1"] = "1";       // Content valid, Type wrong
        $_GET["test2"] = "True";    // Content sanitizable, Type wrong
        $_GET["test3"] = true;      // Content valid, Type valid
        $_GET["test4"] = "ABC";     // Content wrong, Type wrong
        $valid = true;
        
        $si = new SecureInput(true, true);
        
        // Test various int types
        $this->assertSame($valid, $si->get("test1", SecureInput::SI_TYPE_BOOL, "fail", false, false));      // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", SecureInput::SI_TYPE_BOOL, "fail", false, false));      // Test cast content - Negative
        //$this->assertSame($valid, $si->get("test2", SecureInput::SI_TYPE_BOOL, "fail", true, false));      // Test sanitized content - Positive
        $this->assertSame("fail", $si->get("test4", SecureInput::SI_TYPE_BOOL, "fail", true, false));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", SecureInput::SI_TYPE_BOOL, "fail", false, true));      // Test strict content - Positive
        $this->assertSame("fail", $si->get("test1", SecureInput::SI_TYPE_BOOL, "fail", false, true));      // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", SecureInput::SI_TYPE_BOOL, "fail", false, false));      // Test wrong content
    }
    
    public function testGetInt()
    {
    	$_GET["test1"] = "1234";    // Content valid, Type wrong
        $_GET["test2"] = "1234ABC"; // Content sanitizable, Type wrong
        $_GET["test3"] = 1234;      // Content valid, Type valid
        $_GET["test4"] = "ABC";  // Content wrong, Type wrong
        $valid = 1234;
        $type = SecureInput::SI_TYPE_INT;
        
        $si = new SecureInput(true, true);
        
        // Test various int types
        $this->assertSame($valid, $si->get("test1", $type, "fail", false, false));      // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));      // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        //$this->assertSame("fail", $si->get("test4", $type, "fail", true, false));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        $this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        //$this->assertSame("fail", $si->get("test4", $type, "fail", false, false));      // Test wrong content
    }    

    public function testGetFloat()
    {
        $_GET["test1"] = "12.34";      // Content valid, Type wrong
        $_GET["test2"] = "12.34ABC";   // Content sanitizable, Type wrong
        $_GET["test3"] = 12.34;        // Content valid, Type valid
        $_GET["test4"] = "ABC";      // Content wrong, Type wrong
        $valid = 12.34;
        $type = SecureInput::SI_TYPE_FLOAT;
        
        $si = new SecureInput(true, true);
        
        // Test various int types
        $this->assertSame($valid, $si->get("test1", $type, "fail", false, false));      // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));      // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        //$this->assertSame("fail", $si->get("test4", $type, "fail", true, false));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        $this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        //$this->assertSame("fail", $si->get("test4", $type, "fail", false, false));      // Test wrong content
    }    
    
    public function testGetDate()
    {
        $_GET["test1"] = "2010-12-31 13:56:34";             // Content valid, Type wrong
        $_GET["test2"] = "2010-12-31T13:56:34";             // Content sanitizable, Type wrong
        $_GET["test3"] = "2010-12-31 13:56:34";             // Content valid, Type valid
        $_GET["test4"] = "2010-14-15 11:22:33";             // Content wrong, Type wrong
        $valid         = "2010-12-31 13:56:34";
        $type          = SecureInput::SI_TYPE_DATE;
        
        $si = new SecureInput(true, true);
        
        // Test various date types
        $this->assertSame($valid, $si->get("test1", $type, "fail", false, false));      // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));      // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        $this->assertSame("fail", $si->get("test4", $type, "fail", true, false));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        //$this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", $type, "fail", false, false));      // Test wrong content
    }       

    public function testGetString()
    {
        $_GET["test1"] = 1234;         // Content valid, Type wrong
        $_GET["test2"] = 1234;       // Content sanitizable, Type wrong
        $_GET["test3"] = "1234";       // Content valid, Type valid
        $_GET["test4"] = array();      // Content wrong, Type wrong
        $valid = "1234";
        $type = SecureInput::SI_TYPE_STRING;
        
        $si = new SecureInput(true, true);
        
        // Test various string types
        //$this->assertSame($valid, $si->get("test1", $type, "fail", false, false, SecureInput::SI_SOURCE_REQUEST, true));   // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false, SecureInput::SI_SOURCE_REQUEST, true));     // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false, SecureInput::SI_SOURCE_REQUEST, true));      // Test sanitized content - Positive
        //$this->assertSame("fail", $si->get("test4", $type, "fail", true, false, SecureInput::SI_SOURCE_REQUEST, true));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true, SecureInput::SI_SOURCE_REQUEST, true));      // Test strict content - Positive
        $this->assertSame("fail", $si->get("test1", $type, "fail", false, true, SecureInput::SI_SOURCE_REQUEST, true));      // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", $type, "fail", false, false, SecureInput::SI_SOURCE_REQUEST, true));     // Test wrong content
    }        

    public function testGetArray()
    {
        //$_GET["test1"] = 1234;          // Content valid, Type wrong
        //$_GET["test2"] = "1234";        // Content sanitizable, Type wrong
        $_GET["test3"] = array("1234");   // Content valid, Type valid
        $_GET["test4"] = "1234";          // Content wrong, Type wrong
        $valid = array("1234");
        $type = SecureInput::SI_TYPE_ARRAY;
        
        $si = new SecureInput(true, true);
        
        // Test various int types
        //$this->assertSame($valid, $si->get("test1", $type, "fail", false, false, SecureInput::SI_SOURCE_REQUEST, true));      // Test cast content - Positive
        //$this->assertSame("fail", $si->get("test2", $type, "fail", false, false, SecureInput::SI_SOURCE_REQUEST, true));      // Test cast content - Negative
        //$this->assertSame($valid, $si->get("test2", $type, "fail", true, false, SecureInput::SI_SOURCE_REQUEST, true));      // Test sanitized content - Positive
        $this->assertSame("fail", $si->get("test4", $type, "fail", true, false, SecureInput::SI_SOURCE_REQUEST, true));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true, SecureInput::SI_SOURCE_REQUEST, true));      // Test strict content - Positive
        //$this->assertSame("fail", $si->get("test1", $type, "fail", false, true, SecureInput::SI_SOURCE_REQUEST, true));      // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", $type, "fail", false, false, SecureInput::SI_SOURCE_REQUEST, true));      // Test wrong content
    }            

    public function testGetXML()
    {
        $_GET["test1"] = "<?xml version=\"1.0\"?>\n<test>Hallo</test>\n";              // Content valid, Type wrong
        $_GET["test2"] = "<test>Hallo</test>";                                         // Content sanitizable, Type wrong
        $_GET["test3"] = "<?xml version=\"1.0\"?>\n<test>Hallo</test>\n";              // Content valid, Type valid
        $_GET["test4"] = "<test>Hallo</hallo>";                                        // Content wrong, Type wrong
        $valid = "<?xml version=\"1.0\"?>\n<test>Hallo</test>\n";
        $type          = SecureInput::SI_TYPE_XML;

        $si = new SecureInput(true, true);
        
        // Test various date types
        $this->assertSame($valid, $si->get("test1", $type, "fail", false, false));   // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));   // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));    // Test sanitized content - Positive
        $this->assertSame("fail", $si->get("test4", $type, "fail", true, false));    // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));    // Test strict content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, true));    // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", $type, "fail", false, false));   // Test wrong content
    }     

    public function testGetPlain()
    {
        $_GET["test1"] = 1234;         // Content valid, Type wrong
        $_GET["test2"] = "12<A>34";     // Content sanitizable, Type wrong
        $_GET["test3"] = "1234";       // Content valid, Type valid
        $_GET["test4"] = array();      // Content wrong, Type wrong
        $valid = "1234";
        $type = SecureInput::SI_TYPE_PLAIN;
        
        $si = new SecureInput(true, true);
        
        // Test various string types
        $this->assertSame($valid, $si->get("test1", $type, "fail", false, false));     // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));     // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        //$this->assertSame("fail", $si->get("test4", $type, "fail", true, false));    // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        $this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        //$this->assertSame("fail", $si->get("test4", $type, "fail", false, false));   // Test wrong content
    }     

    public function testGetPlainExt()
    {
        $_GET["test1"] = 1234;                                        // Content valid, Type wrong
        $_GET["test2"] = "AbcdEfg<H>93745,.-;:_+#%\${&/ () ÓÛßåêñòúŕČ";    // Content sanitizable, Type wrong
        $_GET["test3"] = "AbcdEfgH93745,.-;:_+#%&/ () ÓÛßåêñòúŕČ";         // Content valid, Type valid
        $_GET["test4"] = array();      // Content wrong, Type wrong
        $valid = "AbcdEfgH93745,.-;:_+#%&/ () ÓÛßåêñòúŕČ";
        $type = SecureInput::SI_TYPE_PLAINEXT;
        
        $si = new SecureInput(true, true);
        
        // Test various string types
        $this->assertSame("1234", $si->get("test1", $type, "fail", false, false));     // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));     // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        //$this->assertSame("fail", $si->get("test4", $type, "fail", true, false));    // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        $this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        //$this->assertSame("fail", $si->get("test4", $type, "fail", false, false));   // Test wrong content
    }     
    
    public function testGetEnc()
    {
        $_GET["test1"] = 1234;                              // Content valid, Type wrong
        $_GET["test2"] = "<test>Hallo</test>";              // Content sanitizable, Type wrong
        $_GET["test3"] = "Hallo";                           // Content valid, Type valid
        $_GET["test4"] = "Hallo";                           // Content wrong, Type wrong
        $type          = SecureInput::SI_TYPE_ENC;
        
        $si = new SecureInput(true, true);
        
        // Test various date types
        $this->assertSame("1234", $si->get("test1", $type, "fail", false, false));                                   // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));                                   // Test cast content - Negative
        $this->assertSame("&#60;test&#62;Hallo&#60;/test&#62;", $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        //$this->assertSame("fail", $si->get("test4", $type, "fail", true, false));                                  // Test sanitized content - Negative
        $this->assertSame("Hallo", $si->get("test3", $type, "fail", false, true));                                   // Test strict content - Positive
        $this->assertSame("fail", $si->get("test1", $type, "fail", false, true));                                    // Test strict content - Negative
        //$this->assertSame("fail", $si->get("test4", $type, "fail", false, false));                                 // Test wrong content
    }          

    public function testGetEmail()
    {
        $_GET["test1"] = "test@test.de";      // Content valid, Type wrong
        $_GET["test2"] = "te<>st@tesüt.de";  // Content sanitizable, Type wrong
        $_GET["test3"] = "test@test.de";      // Content valid, Type valid
        $_GET["test4"] = "hallo";             // Content wrong, Type wrong
        $valid = "test@test.de";
        $type = SecureInput::SI_TYPE_EMAIL;

        $si = new SecureInput(true, true);
        
        // Test various string types
        //$this->assertSame($valid, $si->get("test1", $type, "fail", false, false));     // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));     // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        $this->assertSame("fail", $si->get("test4", $type, "fail", true, false));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        //$this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", $type, "fail", false, false));   // Test wrong content
    }      
    
    public function testGetURL()
    {
        $_GET["test1"] = "http://www.google.de";         // Content valid, Type wrong
        $_GET["test2"] = "http://user:pass@www.google.de?hallo";   // Content sanitizable, Type wrong
        $_GET["test3"] = "http://www.google.de";         // Content valid, Type valid
        $_GET["test4"] = "hallo<a>";                 // Content wrong, Type wrong
        $valid = "http://www.google.de";
        $type = SecureInput::SI_TYPE_URL;

        $si = new SecureInput(true, true);
        
        // Test various string types
        //$this->assertSame($valid, $si->get("test1", $type, "fail", false, false));     // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));     // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        $this->assertSame("fail", $si->get("test4", $type, "fail", true, false));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        //$this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", $type, "fail", false, false));   // Test wrong content
    }    
     
    public function testGetURLQ()
    {
        $_GET["test1"] = "http://www.google.de?hallo";         // Content valid, Type wrong
        $_GET["test2"] = "http://user:pass@www.google.de?hallo";   // Content sanitizable, Type wrong
        $_GET["test3"] = "http://www.google.de?hallo";         // Content valid, Type valid
        $_GET["test4"] = "hallo<a>";                 // Content wrong, Type wrong
        $valid = "http://www.google.de?hallo";
        $type = SecureInput::SI_TYPE_URLQ;

        $si = new SecureInput(true, true);
        
        // Test various string types
        //$this->assertSame($valid, $si->get("test1", $type, "fail", false, false));     // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));     // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        $this->assertSame("fail", $si->get("test4", $type, "fail", true, false));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        //$this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", $type, "fail", false, false));   // Test wrong content
    }    
    
    public function testGetURLL()
    {
        $_GET["test1"] = "http://user:pass@www.google.de";         // Content valid, Type wrong
        $_GET["test2"] = "http://user:pass@www.google.de?hallo";   // Content sanitizable, Type wrong
        $_GET["test3"] = "http://user:pass@www.google.de";         // Content valid, Type valid
        $_GET["test4"] = "hallo<a>";                 // Content wrong, Type wrong
        $valid = "http://user:pass@www.google.de";
        $type = SecureInput::SI_TYPE_URLL;

        $si = new SecureInput(true, true);
        
        // Test various string types
        //$this->assertSame($valid, $si->get("test1", $type, "fail", false, false));     // Test cast content - Positive
        $this->assertSame("fail", $si->get("test2", $type, "fail", false, false));     // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        $this->assertSame("fail", $si->get("test4", $type, "fail", true, false));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        //$this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", $type, "fail", false, false));   // Test wrong content
    }  
    

    public function testGetURLQL()
    {
        $_GET["test1"] = "http://user:pass@www.google.de?hallo";         // Content valid, Type wrong
        $_GET["test2"] = "http://user:pass@www.google.de?hallo";         // Content sanitizable, Type wrong
        $_GET["test3"] = "http://user:pass@www.google.de?hallo";         // Content valid, Type valid
        $_GET["test4"] = "hallo<a>";                                     // Content wrong, Type wrong
        $valid = "http://user:pass@www.google.de?hallo";
        $type = SecureInput::SI_TYPE_URLQL;

        $si = new SecureInput(true, true);
        
        // Test various string types
        //$this->assertSame($valid, $si->get("test1", $type, "fail", false, false));     // Test cast content - Positive
        //$this->assertSame("fail", $si->get("test2", $type, "fail", false, false));     // Test cast content - Negative
        $this->assertSame($valid, $si->get("test2", $type, "fail", true, false));      // Test sanitized content - Positive
        $this->assertSame("fail", $si->get("test4", $type, "fail", true, false));      // Test sanitized content - Negative
        $this->assertSame($valid, $si->get("test3", $type, "fail", false, true));      // Test strict content - Positive
        //$this->assertSame("fail", $si->get("test1", $type, "fail", false, true));      // Test strict content - Negative
        $this->assertSame("fail", $si->get("test4", $type, "fail", false, false));   // Test wrong content
    }      
    
}
?>