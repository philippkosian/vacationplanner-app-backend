<?php

class Urlaub {
    
    protected $dblink = null;
    
    public function __construct($dblink){
        $this->dblink=$dblink;
    }
    
    public function getList () {
        
        $sql = "SELECT * FROM urlaub";
 
        $db_erg = mysqli_query($this->dblink, $sql);
        if (! $db_erg) {
            die('Ungültige Abfrage: ' . mysqli_error());
        }

        $list = array();
        while ($zeile = mysqli_fetch_array($db_erg, MYSQLI_ASSOC))
        {
            $list[] = $zeile;
        }
        mysqli_free_result($db_erg);
        return $list;
    }

    public function getListForMonth ($yearmonth) {
        
        $sql = "SELECT * FROM urlaub WHERE date LIKE '%$yearmonth%'";
 
        $db_erg = mysqli_query($this->dblink, $sql);
        if (! $db_erg) {
            die('Ungültige Abfrage: ' . mysqli_error());
        }

        $list = array();
        while ($zeile = mysqli_fetch_array($db_erg, MYSQLI_ASSOC))
        {
            $list[] = $zeile;
        }
        mysqli_free_result($db_erg);
        return $list;
    }

    public function createHolliday($user_id, $date) {
        $sql = "INSERT INTO urlaub (`user_id`,`date`) VALUES ('$user_id' , '$date')";
        if (mysqli_query($this->dblink, $sql)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function deleteHolliday($user_id, $date) {
        $sql = "DELETE FROM urlaub WHERE user_id = '$user_id' AND date = '$date'";
        if (mysqli_query($this->dblink, $sql)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function hasHolliday ($user_id, $date) {

        $sql = "SELECT * FROM urlaub WHERE user_id = '$user_id' AND date = '$date'";

        $db_erg = mysqli_query($this->dblink, $sql);
        if (! $db_erg) {
            die('Ungültige Abfrage: ' . mysqli_error());
        }
        if (mysqli_fetch_array( $db_erg, MYSQLI_ASSOC)) {
            return true;
        } else {
            return false;   
        }
        mysqli_free_result($db_erg);
    }
}

?>