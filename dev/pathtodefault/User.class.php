<?php

class User {
    
    protected $dblink = null;
    
    function __construct($dblink){
        $this->dblink=$dblink;
    }

    public function getList ($group) {
        
        $sql = "SELECT * FROM users WHERE active = 1";

        if ( !empty($group) ) {

                $sql .= ' AND `usergroup` = "'.$group.'"';

        }

        $db_erg = mysqli_query($this->dblink, $sql);
        
        if (! $db_erg) {
            die('Ungültige Abfrage: ' . mysqli_error($this->dblink));
        }
        
        $list = array();
        
        while ($zeile = mysqli_fetch_array($db_erg, MYSQLI_ASSOC)) {
            $list[] = $zeile;
        }
        
        mysqli_free_result( $db_erg );   
        return $list;
    }

    // public function getListByGroups ($groupselect) {
    //     $sql = "SELECT * FROM users WHERE group = '$groupselect'";
 
    //     $db_erg = mysqli_query($this->dblink, $sql);
        
    //     if (! $db_erg) {
    //         die('Ungültige Abfrage: ' . mysqli_error());
    //     }
        
    //     $grouplist = array();
        
    //     while ($zeile = mysqli_fetch_array($db_erg, MYSQL_ASSOC)) {
    //         $grouplist[] = $zeile;
    //     }
        
    //     mysqli_free_result( $db_erg );   
    //     return $grouplist;
    // }

    public function getUserType ($user_id) {
        $sql = "SELECT usertype FROM users WHERE user_id = '$user_id'";

        $db_erg = mysqli_query($this->dblink, $sql);

        if (! $db_erg) {
            die('Ungültige Abfrage: ' . mysqli_error());
        }

        $usertype = false;
        while ($zeile = mysqli_fetch_array($db_erg, MYSQLI_ASSOC)) {
            $usertype = $zeile;
        }

        mysqli_free_result($db_erg);
        return $usertype;
    }

    public function deactivateUser($user_id) {
        $sql = "UPDATE users SET active=0 WHERE user_id='$user_id'";

        if (mysqli_query($this->dblink, $sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteUser($user_id) {
        $sql = "DELETE FROM users WHERE user_id='$user_id'";
        if (mysqli_query($this->dblink, $sql)) {
            return true;
        } else {
            return false;
        }
    }
	
	public function getUserByEmail ($email) {
        $sql = "SELECT * FROM users WHERE email = '$email'";

        $db_erg = mysqli_query($this->dblink, $sql);

        if (! $db_erg) {
            die('Ungültige Abfrage: ' . mysqli_error());
        }

        $userdata = false;
        while ($zeile = mysqli_fetch_array($db_erg, MYSQLI_ASSOC)) {
            $userdata = $zeile;
        }

        mysqli_free_result($db_erg);
        return $userdata;
    }
	
	public function getUserByName ($name) {
        $sql = "SELECT * FROM users WHERE name = '$name' limit 1";

        $db_erg = mysqli_query($this->dblink, $sql);

        if (! $db_erg) {
            die('Ungültige Abfrage: ' . mysqli_error());
        }

        $userdata = false;
        while ($zeile = mysqli_fetch_array($db_erg, MYSQLI_ASSOC)) {
            $userdata = $zeile;
        }

        mysqli_free_result($db_erg);
        return $userdata;
    }

    public function getUserByGroup($group) {
        $sql = "SELECT * FROM users WHERE group = '$group'";

        $db_erg = mysqli_query($this->dblink, $sql);

        if (! $db_erg) {
            die('Ungültige Abfrage: ' . mysqli_error());
        }

        $userdata = false;
        while ($zeile = mysqli_fetch_array($db_erg, MYSQLI_ASSOC)) {
            $userdata = $zeile;
        }

        mysqli_free_result($db_erg);
        return $userdata;
    }
	
	public function registerUser ($name, $email, $token, $password_hash, $groupselector) {
		
		$sql = "INSERT INTO users (name, email, activationtoken, password, group) VALUES ('$name', '$email', '$token', '$password_hash', '$groupselector')";

		if (mysqli_query($this->dblink, $sql)) {
			return true;
		} else {
			return false;
		}
        
    }

    public function registerPasswordResetToken ($passwrToken, $email) {
        
		$sql = "UPDATE users SET passwordresettoken = '$passwrToken' WHERE email = '$email'"; //WHERE $email(form input) = email(from db)

        if (mysqli_query($this->dblink, $sql)) {
            return true;
        } else {
            return false;
        }
    }
	
	public function activateAccount ($getMailFromUrl) {
		
		$sql = "UPDATE users SET activated = 1 WHERE email = '$getMailFromUrl'";
		
		$db_erg = mysqli_query($this->dblink, $sql);

        if (! $db_erg) {
            die('Ungültige Abfrage: ' . mysqli_error());
        }

        $userdata = false;
		
        while ($zeile = mysqli_fetch_array($db_erg, MYSQLI_ASSOC)) {
            $userdata = $zeile;
        }

        mysqli_free_result($db_erg);
        return $userdata;
		
	}

    public function resetPassword ($getMailFromUrl) {
        $sql = "UPDATE users SET password = NULL WHERE email = '$getMailFromUrl'";
		
        // if (! $db_erg) {
            // die('Ungültige Abfrage: ' . mysqli_error());
        // }

        if (mysqli_query($this->dblink, $sql)) {
            return true;
        } else {
            return false;
        }

    }

    public function setTheNewPassword ($password_hash, $email) {

		$sql = "UPDATE users SET password = '$password_hash' WHERE email = '$email'";

        if (mysqli_query($this->dblink, $sql)) {
            return true;
        } else {
            return false;
        }

    }

}

?>