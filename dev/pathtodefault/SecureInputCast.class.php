<?php
/**
 * SecureInputCast library
 * provides type castng functions as specified and used in SecureInput class
 *
 * @package      SecureInput
 * @author       Tobias Bulla <tobias.bulla@publicis.de>
 * @version      SVN: $Id: cdaContentItem.type.php 376 2010-03-23 12:52:25Z bulla $
 * @modifiedby   $LastChangedBy: bulla $
 * @revision     $LastChangedRevision: 376 $
 * @lastmodified $LastChangedDate: 2010-03-23 13:52:25 +0100 (Di, 23 Mrz 2010) $
 * @license      http://www.publicis.de
 * @link         http://wiki.publicis.de/index.php/Category:SecureInput
 * @link         $HeadURL: https://siemens.svn.publicis.de/svn/siemens_apps/cda2/trunk/common/cdaContentItem.type.php $
 */

/**
 * SecureInputCast
 * provides type castng functions as specified and used in SecureInput class
 *
 * @package SecureInput
 */
class SecureInputCast
{
	// Valid values for type bool
	static public $BoolTrue  = array(1, -1, "1", "-1", "true", "TRUE");
	static public $BoolFalse = array(0, "0", "false", "FALSE");
	

	/**
	 * check if type of value is bool
	 *
	 * @param mixed $value The value to validate.
	 * @return bool true if the value has the correct type, false otherwise.
	 */
	static public function isBool($value)
	{
		return is_bool($value)  || in_array($value, self::$BoolTrue, true)  || in_array($value, self::$BoolFalse, true);
	}

	/**
	 * check if type of value is integer
	 *
	 * @param mixed $value The value to validate.
	 * @return bool true if the value has the correct type, false otherwise.
	 */
	static public function isInt($value)
	{
		return is_int($value)   || is_numeric($value);
	}

	/**
	 * check if type of value is float
	 *
	 * @param mixed $value The value to validate.
	 * @return bool true if the value has the correct type, false otherwise.
	 */
	static public function isFloat($value)
	{
		return is_float($value) || is_numeric($value);
	}

	/**
	 * check if type of value is datetime or a string convertible to datetime
	 *
	 * @param mixed $value The value to validate.
	 * @return bool true if the value has the correct type, false otherwise.
	 */
	static public function isDate($value)
	{
		$DateTime = (class_exists("DateTime", false) && ($value instanceof DateTime));
		return $DateTime  || (strtotime($value) > 0);
	}

	/**
	 * check if type of value is string
	 *
	 * @param mixed $value The value to validate.
	 * @return bool true if the value has the correct type, false otherwise.
	 */
	static public function isString($value)
	{
		return is_string($value);
	}

	/**
	 * check if type of value is array
	 *
	 * @param mixed $value The value to validate.
	 * @return bool true if the value has the correct type, false otherwise.
	 */
	static public function isArray($value)
	{
		return is_array($value);
	}

	/**
	 * check if type of value is valid XML
	 *
	 * @param mixed $value The value to validate.
	 * @return bool true if the value has the correct type, false otherwise.
	 */
	static public function isXML($value)
	{
		return (@simplexml_load_string($value) !== false);
	}

	/**
	 * try to convert type of value to bool
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
	 */
	static public function toBool($value, &$valid = null)
	{
		$valid = self::isBool($value);

		if(is_bool($value))
		    return $value;
		if(in_array($value, self::$BoolTrue, true))
		    return true;
		if(in_array($value, self::$BoolFalse, true))
		    return false;

		return null;
	}

	/**
	 * try to convert type of value to integer
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
	 */
	static public function toInt($value, &$valid = null)
	{
		$valid = self::isInt($value);
		return (int)$value;
	}

	/**
	 * try to convert type of value to float
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
	 */
	static public function toFloat($value, &$valid = null)
	{
		$valid = self::isFloat($value);
		return (float)$value;
	}

	/**
	 * try to convert type of value to DateTime
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
	 */
	static public function toDate($value, &$valid = null, $format = "Y-m-d H:i:s")
	{
		$valid = self::isDate($value);
		return $valid ? date($format, strtotime($value)) : null;
	}

	/**
	 * try to convert type of value to string
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
	 */
	static public function toString($value, &$valid = null)
	{
		$valid = self::isString($value);
		return (string)$value;
	}

    /**
     * try to convert type of value to an array
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
     */
    static public function toArray($value, &$valid = null)
    {
        $valid = self::isArray($value);
        $cast = $valid ? (array)$value : null;
        return $cast;
    }

	/**
	 * try to convert type of value to XML string
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
	 */
	static public function toXML($value, &$valid = null)
	{
		$valid = self::isXML($value);
		$cast = @simplexml_load_string($value);
		return $cast ? $cast->asXML() : null;
	}

	/**
	 * try to convert type of value to plain string with letters between 32 and 128 ASCII
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
	 */
	static public function toStringPlain($value, &$valid = null)
	{
		$original = $value;
		$cast  = filter_var($value, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
		$valid = ($cast == $original);
		return $cast;
	}

    /**
     * try to convert type of value to plain string with whitelisted letters from any language
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
     */
    static public function toStringPlainExt($value, &$valid = null)
    {
		mb_internal_encoding("UTF-8");
		mb_regex_encoding("UTF-8");

    	$white   = '\*,\.\-\;\:\_\+\#\%\&\/ \(\)\@\r\n'                                             // Special characters
    	         . 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'                 // Normal ASCII letters and numbers
    	         . 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿŔŕŠšĐjdŽžČčĆćƒ';  // Foreign UTF-8 letters

        $original = $value;
        $cast  = mb_ereg_replace('[^'.$white.']', '', $value);
        $valid = ($cast == $original);
        return $cast;
    }
    
    /**
    * try to convert type of value to plain string with whitelisted letters from any language and comment e. g. ?!
    * @param mixed $value     input value
    * @param bool $valid      (Passed by Reference) Result of validation
    * @return string          converted / cleaned input string or null
    */
    static public function toStringPlainExtComment($value, &$valid = null)
    {
    	$white   = '\~\|\*,\.\-\;\:\_\+\#\%\&\/ \(\)\@\€\r\n?!\"\'\='                                     // Special characters
    				. 'a-zA-Z0-9'																	// Basic ASCII and numbers
    				. self::getRanges();  		          											// Defined ranges
    
    	$original = $value;
    	$cast  = preg_replace('/[^'.$white.']/u', '', $value);
    	$valid = ($cast == $original);
    	return $cast;
    }
    
    /**
    * try to convert type of value to plain string with whitelisted letters from any language and " ' (for phrase search)
    * @param mixed $value     input value
    * @param bool $valid      (Passed by Reference) Result of validation
    * @return string          converted / cleaned input string or null
    */
    static public function toStringPlainExtSearch($value, &$valid = null)
    {
    	$white   = '\-\+\:\&\"\'\@\r\n\. '                                     								// Special characters
    				. 'a-zA-Z0-9'																	// Basic ASCII and numbers
    				. self::getRanges();  		          											// Defined ranges
    
    	$original = $value;
    	$cast  = preg_replace('/[^'.$white.']/u', '', $value);
    	$valid = ($cast == $original);
    	return $cast;
    }

	/**
	 * try to convert type of value to emnail string
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
	 */
	static public function toStringEmail($value, &$valid = null)
	{
		$original = $value;
		$cast  = filter_var($value, FILTER_SANITIZE_EMAIL);
		$cast  = filter_var($cast, FILTER_VALIDATE_EMAIL) ? $cast : null;
		$valid = ($cast == $original);
		return $cast;
	}

	/**
	 * try to convert type of value to encoded string. Encodes letters above 128 and removes letters below 32 ASCII
     * @param mixed $value     input value
     * @param bool $valid      (Passed by Reference) Result of validation
     * @return string          converted / cleaned input string or null
	 */
	static public function toStringEnc($value, &$valid = null)
	{
		$original = $value;
		$cast  = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_ENCODE_HIGH | FILTER_FLAG_STRIP_LOW);
		$valid = ($cast == $original);
		return $cast;
	}

	/**
	 * try to convert type of value to url string
	 * @param mixed $value     input value
	 * @param bool $valid      (Passed by Reference) Result of validation
	 * @param string $type     one of the four URL type constants (defaults to SecureInput::SI_TYPE_URLQ)
	 * @return string          converted / cleaned input string or null
	 */
	static public function toStringURL($value, &$valid = null, $type = SecureInput::SI_TYPE_URLQ)
	{
		$original = $value;

		$value = filter_var($value, FILTER_SANITIZE_URL);
		$info = @parse_url(filter_var($value, FILTER_SANITIZE_URL));

		$valid = $info ? true : false;
		if($valid == false) return null;

		$cast  = isset($info["scheme"]) ? $info["scheme"]."://" : "";
		$cast .= (($type == SecureInput::SI_TYPE_URLL || $type == SecureInput::SI_TYPE_URLQL) && isset($info["user"])) ? $info["user"] : "";
		$cast .= (($type == SecureInput::SI_TYPE_URLL || $type == SecureInput::SI_TYPE_URLQL) && isset($info["pass"])) ? ":".$info["pass"] : "";
		$cast .= ($type == SecureInput::SI_TYPE_URLL || $type == SecureInput::SI_TYPE_URLQL) ? "@" : "";
		$cast .= isset($info["host"]) ? $info["host"] : "";
		$cast .= isset($info["path"]) ? $info["path"] : "";
		$cast .= (($type == SecureInput::SI_TYPE_URLQ || $type == SecureInput::SI_TYPE_URLQL) && isset($info["query"])) ? "?".$info["query"] : "";
		$cast .= (isset($info["fragment"])) ? "#".$info["fragment"] : "";

		$cast = filter_var($cast, FILTER_VALIDATE_URL) ? $cast : null;

		$valid = $valid || $cast == $original;
		return $cast;
	}
	
	static private function getRanges(){
		// Define Unicode Ranges in hex to cover up large areas of allowed characters
		// Activate the ranges by commenting out/in
		// see: http://www.alanwood.net/unicode/
		//      http://www.ftrain.com/unicode/
		$u_ranges = array();
		$u_ranges['latin']      = array(
				array('00C0', '00FF'),  // 0: Latin-1 Supplement (German, Italian Portugese, Spanish, Swedish, etc.)
				array('0100', '017F')   // 1: Latin Extended A
		);
		$u_ranges['cyrillic']   = array(
				array('0400','04FF'),   // 0: Common
				array('0500', '052F')   // 1: Supplement
		);
		$u_ranges['greek']      = array(
				array('0370', '03FF')   // 0: Common
		);
		$u_ranges['han']        = array(
				array('4E00', '9FFF'),  // 0: Common
				array('3400', '4DFF'),  // 1: Rare
				array('20000', '2A6DF'),// 2: Rare, historic
				array('F900', 'FAFF'),  // 3: Duplicates, unifiable variants, corporate characters
				array('2F800', '2FA1F') // 4: Unifiable variants
		);
		$u_ranges['japanese']   = array(
				array('4E00','9FBF'),   // 0: Kanji, adopted from Chinese, see $u_ranges['han'][0];
				array('3040', '309F'),  // 1: Hiragana
				array('30A0', '30FF')   // 2: Katagana
		);
			
		$merged = "";
		foreach ($u_ranges as $script){
			if(!is_array($script)){
				continue;
			}
			foreach ($script as $var){
				$merged .= sprintf('\\x{%s}-\\x{%s}', $var[0], $var[1]);
			}
		}
		return $merged;
	}
}