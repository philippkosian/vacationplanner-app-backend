<?php

session_start();

require_once ('database-files/datenbank-verbindung.php');
require_once ('database-files/User.class.php');
require_once ('database-files/Urlaub.class.php');
require_once ('password_compat-1.0.4/lib/password.php');
require_once 'tokenGen.php';
include_once "SecureInput/SecureInput.class.php";

$SecureInput = new SecureInput(false);

$GET = $SecureInput->get(1, SecureInput::SI_SOURCE_GET);
$POST = $SecureInput->get(2, SecureInput::SI_SOURCE_POST);
$COOKIE = $SecureInput->get(4, SecureInput::SI_SOURCE_COOKIE);
$REQUEST = $SecureInput->get(3, SecureInput::SI_SOURCE_REQUEST);

$name = $SecureInput->get("name", SecureInput::SI_TYPE_PLAINEXT, NULL);
$email = $SecureInput->get("email", SecureInput::SI_TYPE_EMAIL, NULL);
$password = $SecureInput->get("password", SecureInput::SI_TYPE_PLAINEXT, NULL);
$password2 = $SecureInput->get("password2", SecureInput::SI_TYPE_PLAINEXT, NULL);

$user = new User($db_link);

use Utils\RandomStringGenerator;

?>

<!DOCTYPE html>

	<head>

		<!-- Required Prerequisites -->
		<script type="text/javascript" src="usefulls/jquery/dist/jquery.min.js"></script>
		<script type="text/javascript" src="usefulls/moment/min/moment.min.js"></script>
		<link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css">

		<!-- Hover Effects -->
		<link rel="stylesheet" href="usefulls/hover-effects-master/css/hover.css">

		<!-- Custom CSS -->
	    <link rel="stylesheet" href="css/login_register.css">

	    <!-- font-awesome -->
	    <link rel="stylesheet" href="usefulls/font-awesome-4.6.3/css/font-awesome.css">

	    <!-- Bootstrap select -->
	    <script type="text/javascript" src="usefulls/bootstrap-select/dist/js/bootstrap-select.js"></script>
	    <link rel="stylesheet" href="usefulls/bootstrap-select/dist/css/bootstrap-select.css"/>

		<!-- Include Date Range Picker -->
		<script type="text/javascript" src="usefulls/bootstrap-daterangepicker-master/daterangepicker.js"></script>
		<link rel="stylesheet" href="usefulls/bootstrap-daterangepicker-master/daterangepicker.css"/>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
		 
		<!-- The Main JS -->
		<script type="text/javascript" src="js/main.js"></script>

		<title>Registration</title>	

	</head>

	<body>
		<div class="container">

			<div class="row">

				<div class="col-md-12">
					<h1 class="">Registration</h1>
				</div>
				
				<?php
					$showFormular = true; //Variable ob das Registrierungsformular anezeigt werden soll
					if($showFormular) {
				?>

			</div>

			<form class="input-form-lg form-group-lg form-signin" action="?register=1" method="post" role="login">
				
				<div>

					<div class="row margin-bottom-1">
						<div class="col-md-12">
							<input name="name" class="form-control input-lg text-center" type="text" placeholder="name" value="<?php echo $name;?>">
						</div>
					</div>

					<div class="row margin-bottom-1">
						<div class="col-md-12">
							<label class="sr-only">Email</label>
							<input name="email" class="form-control input-lg text-center" type="email" placeholder="email" value="<?php echo $email;?>">
						</div>
					</div>

					<div class="row margin-bottom-1">
						<div class="col-md-12">
							<label class="sr-only">Password</label>
							<input name="password" class="form-control input-lg text-center" type="password" placeholder="password">
						</div>
					</div>

					<div class="row margin-bottom-1">
						<div class="col-md-12">
							<label class="sr-only">Password</label>
							<input name="password2" class="form-control input-lg text-center" type="password" placeholder="repeat password">
						</div>
					</div>

					<div class="row margin-bottom-1">
						<div class="col-md-12">
							<span title="Create user">
								<button class="btn btn-lg btn-primary btn-block hvr-glow" type="submit" value="Abschicken">
									Create User &nbsp
									<i class="fa fa-user"></i> <i class="fa fa-plus"></i>
								</button>
							</span>
						</div>
					</div>

				</div>

			</form>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Back to Login Button START ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
			<form action="login.php">
				<div class="row">
					<div class="col-md-12 text-center">
						<span title="Back to login">
							<button  name="action" value="logout" type="submit" class="btn btn-lg btn-primary btn-block hvr-shadow">
								Back to login &nbsp
								<i class="fa fa-mail-reply"></i>
							</button>
						</span>
					</div>
				</div>
			</form>	
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Back to Login Button END ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
				<?php
					if(isset($_GET['register'])) {
					
						$error = false;
						
						//print_r($password);
					  
						if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
							echo '<div class="row">'.'<div class="col-md-1">'.'<p class="registration-error-message">&nbspError:</p>'.'</div>';
							echo '<div class="col-md-3">'.'<p class="registration-error-message">- Please enter a valid e-mail address</p>'.'</div>'.'</div>';
							$error = true;
						}
						if(strlen($password) == 0) {
							echo '<div class="row">'.'<div class="col-md-1">'.'<p class="registration-error-message">&nbspError:</p>'.'</div>';
							echo '<div class="col-md-3">'.'<p class="registration-error-message">- A password is missing</p>'.'</div>'.'</div>';
							$error = true;
						}
						if($password != $password2) {
							echo '<div class="row">'.'<div class="col-md-1">'.'<p class="registration-error-message">&nbspError:</p>'.'</div>';
							echo '<div class="col-md-3">'.'<p class="registration-error-message">- The given passwords must be congruent</p>'.'</div>'.'</div>';
							$error = true;
						}
						
						//Überprüfe, ob der Name bereits verwendet wird
						if(!$error) {
						
							$userData = $user->getUserByName($name);
							
							if($userData !== false) {
								echo '<div class="row">'.'<p class="registration-error-message">Error:</p>';
								echo '<div class="col-md-3 registration-error-message">'.'- Something went wrong<br>'.'</div>'.'</div>';
								$error = true;
							}
						}
						
						//Überprüfe, ob die E-Mail-Adresse noch nicht registriert wurde
						if(!$error) {
						
							$userData = $user->getUserByEmail($email);
							
							if($userData !== false) {
								echo '<div class="row">'.'<p class="registration-error-message">Error:</p>';
								echo '<div class="col-md-3 registration-error-message">'.'Something went wrong<br>'.'</div>'.'</div>';
								$error = true;
							}	
						}

						//Keine Fehler, wir können den Nutzer registrieren unter der Bedingung, dass es eine publicis email ist
						if(!$error) {

							$checkForPublicis = 'publicis';
							$pos = strpos($email, $checkForPublicis); //is searching for $checkForPublicis in $email

							if($pos !== false) { //checks if $checkForPublicis is actually in $email
								
								//hash the password
								$password_hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 11));
								
								
								//////////////////////////////////////////////////////////////////////////////////////////////
								///get the token                                                                           ///
								//////////////////////////////////////////////////////////////////////////////////////////////								

								// Create new instance of generator class.
								$generator = new RandomStringGenerator;

								// Set token length.
								$tokenLength = 32;

								// Call method to generate random string/token.
								$token = $generator->generate($tokenLength);
								
								//////////////////////////////////////////////////////////////////////////////////////////////
								///get the token                                                                           ///
								//////////////////////////////////////////////////////////////////////////////////////////////
								

								//////////////////////////////////////////////////////////////////////////////////////////////
								///send email section                                                                      ///
								//////////////////////////////////////////////////////////////////////////////////////////////
								$to      = $email; // Send email to our user
								$subject = 'Signup | Verification'; // Give the email a subject
								$message = 'Thanks for signing up!
								 
Please click this link to activate your account:
https://services.publicis.de/vacation-plan/account-verify.php?user='.$email.'&key='.$token.'kend'; // Our message

								$headers = 'From:noreply.vacation-tool@publicispixelpark.de' . "\r\n"; // Set from headers
								mail($to, $subject, $message, $headers); // Send our email
								//////////////////////////////////////////////////////////////////////////////////////////////
								///send email section                                                                      ///
								//////////////////////////////////////////////////////////////////////////////////////////////

								
								//if everything is parsed in correctly a succes note pops up
								if($user->registerUser($name, $email, $token, $password_hash)) {		
									echo '<div class="row">'.'<div class="col-md-4">'.'<p class="success-message">Registraion was successfull! Check your mail inbox for the verification link</p>'.'</div>'.'</div>';
									$showFormular = false;
								} else { //if not then throw error message
									echo '<div class="row">'.'<p class="registration-error-message">Error:</p>';
									echo '<div class="col-md-3 registration-error-message">'.'Something went wrong with the registration<br>'.'</div>'.'</div>';
								}	
							} else { //if email does not contain 'publicis' in it then throw error message
								echo '<div class="row">'.'<p class="registration-error-message">Error:</p>';
								echo '<div class="col-md-3 registration-error-message">'.'- No access for such email addresses<br>'.'</div>'.'</div>';
								$error = true;	
							}
							
						}
						
					}
					 
				?>
				<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

		</div>
		<?php
		} //Ende von if($showFormular)
		?>
	</body>

</html>