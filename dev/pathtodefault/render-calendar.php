<?php

function render_calendar($usertype) {

	
	//3 usertypes...
	//2=admin
	//1=standarduser
	//0=overview

	ob_start();

	?>

	<div class="row">

		<div class="col-md-12 text-center">

			<p class="hvr-shadow paging">

    			<?php

/**---------------------------------------------------------------------------------------------------------**/
				$db_link = mysqli_connect (MYSQL_HOST, 
				                           MYSQL_BENUTZER, 
				                           MYSQL_PASSWORT, 
				                           MYSQL_DATENBANK);

				$user = new User($db_link);
				$urlaub = new Urlaub($db_link);
				$userlist = $user->getList();

				$pickedDay = '01';

				//days of a week
				$weekdays = array("Su", "Mo", "Tu", "We", "Th", "Fr", "Sa");

				if ( isset($_GET['month']) != true || $_GET['month'] < 1 || $_GET['month'] > 12 ) {
				    $pickedMonth = date('m');
				} else {
				    $pickedMonth = intval ($_GET['month']);
				}

				if ( isset($_GET['year']) != true || $_GET['year'] < 2010) {
				    $pickedYear = date('Y');
				} else {
				    $pickedYear = intval ($_GET['year']);
				}

				$pickedDate= $pickedYear.'-'.$pickedMonth.'-'.$pickedDay;

				$maxDaysMonth = date('t', strtotime($pickedDate));

				//public holidays
				$feiertage = array();
				$feiertage[] = $pickedYear.'-01-01'; // Neujahrstag
				$feiertage[] = $pickedYear.'-01-06'; // Heilige Drei Könige
				$feiertage[] = $pickedYear.'-05-01'; // Tag der Arbeit
				$feiertage[] = $pickedYear.'-08-15'; // Mariä Himmelfahrt
				$feiertage[] = $pickedYear.'-10-03'; // Tag der Deutschen Einheit
				$feiertage[] = $pickedYear.'-11-01'; // Allerheiligen
				$feiertage[] = $pickedYear.'-12-25'; // Erster Weihnachtstag
				$feiertage[] = $pickedYear.'-12-26'; // Zweiter Weihnachtstag

				$tage = 60 * 60 * 24;
				$ostersonntag = easter_date($pickedYear);
				$feiertage[] = date("Y-m-d", $ostersonntag - 2 * $tage);  // Karfreitag
				$feiertage[] = date("Y-m-d", $ostersonntag + 1 * $tage);  // Ostermontag
				$feiertage[] = date("Y-m-d", $ostersonntag + 39 * $tage); // Christi Himmelfahrt
				$feiertage[] = date("Y-m-d", $ostersonntag + 50 * $tage); // Pfingstmontag
				$feiertage[] = date("Y-m-d", $ostersonntag + 60 * $tage); // Fronleichnam
/**---------------------------------------------------------------------------------------------------------**/

		        $pickedMonthName = date('M', strtotime('2001-'.$pickedMonth.'-01'));

		        if ($pickedMonth == 1) {
		            $prevMonth = 12;
		            $prevYear = $pickedYear-1;
		        } else {
		            $prevMonth = $pickedMonth-1;
		            $prevYear = $pickedYear;
		        }
		        
		       if ($pickedMonth == 12) {
		            $nextMonth = 1;
		            $nextYear = $pickedYear+1;
		        } else {
		            $nextMonth = $pickedMonth+1;
		            $nextYear = $pickedYear;
		        }

		        echo '<span title="Previous month">'.'<a href="?day='.$pickedDay.'&month='.$prevMonth.'&year='.$prevYear.'"> <i class="fa  fa-arrow-left"></i> </a>'.'</span>';
		        echo '<span title="Next month">'.'<a href="?day='.$pickedDay.'&month='.$nextMonth.'&year='.$nextYear.'"> <i class="fa  fa-arrow-right"></i> </a>';

				?>

			</p>

			<div class="table-responsive">
				<table class="table custom-calendar margin-top-1">

    				<tr class="row_date">

				        <?php

				            echo'<td colspan="'.($maxDaysMonth+1).'">'.$pickedMonthName.' '.$pickedYear.'</td>';
				            
				        ?>

    				</tr>

    				<tr class="row_date">

        				<td>Name/Date</td>

        				<?php

            			for ($i=1; $i<=$maxDaysMonth; $i++) {
                			$time = strtotime($pickedYear.'-'.$pickedMonth.'-'.$i);
                			echo'<td>'.$i.'. '.$weekdays[date("w", $time)].'</td>';
            			}

        				?>

    				</tr>

    				<?php

	        			foreach ($userlist as $user) {
	            			echo
	            			'<tr class="row_user">
	                		<td> '.$user['name'].'</td>';

		            		for ($i=1; $i<=$maxDaysMonth; $i++) {
				                $time = date ("w", strtotime($pickedYear.'-'.$pickedMonth.'-'.$i));
				                $dateCode = $pickedYear.'-';

				                if ($pickedMonth < 10) {
				                    $dateCode .= '0';
				                }

				                $dateCode .= intval($pickedMonth).'-';

				                if ($i < 10) {
				                    $dateCode .= '0';
				                }

				                $dateCode .= intval($i);

				                if (in_array($dateCode,$feiertage)) {
				                    echo'<td class="feiertag"></td>';
				                } else {

				                if ($time == 0 || $time == 6) {
				                    echo'<td class="weekend"></td>';
				                } else {

									if ($urlaub->hasHolliday($user['user_id'], $pickedYear.'-'.$pickedMonth.'-'.$i)==true) {
									    echo'<td class="holliday">';
									    
									    if((isset($_SESSION) && $user['user_id'] == $_SESSION['userid']) || $usertype == 1) {
									    	echo '<a href="?user_id='.$user['user_id'].'&month='.$pickedMonth.'&datefilter='.$dateCode.' bis '.$dateCode.'&action=delete" class="delete hvr-bounce-out"></a>';
									    }
									    echo '</td>';

									} else {
										echo '<td>';
										if((isset($_SESSION) && $user['user_id'] == $_SESSION['userid']) || $usertype == 1) {
									    	echo '<a href="?user_id='.$user['user_id'].'&month='.$pickedMonth.'&datefilter='.$dateCode.' bis '.$dateCode.'&action=create" class="create hvr-bounce-out"></a>';
										}
										echo '</td>';
									}
				                }

				                }
		            		}
		            		echo'</tr>';
	        			}

    				?>

				</table>
			</div>
		</div>

	</div>

	<?php

	$html = ob_get_clean();
	echo $html;
}
?>