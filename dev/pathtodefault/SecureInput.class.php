<?php
/**
 * SecureInput Interface
 * provides a library to enforce secure input parameters for applications. Replaces $_GET, $_POST, $_REQUEST and $_COOKIE
 *
 * @package      SecureInput
 * @author       Tobias Bulla <tobias.bulla@publicis.de>
 * @version      SVN: $Id: cdaContentItem.type.php 376 2010-03-23 12:52:25Z bulla $
 * @modifiedby   $LastChangedBy: bulla $
 * @revision     $LastChangedRevision: 376 $
 * @lastmodified $LastChangedDate: 2010-03-23 13:52:25 +0100 (Di, 23 Mrz 2010) $
 * @license      http://www.publicis.de
 * @link         http://wiki.publicis.de/index.php/Category:SecureInput
 * @link         $HeadURL: https://siemens.svn.publicis.de/svn/siemens_apps/cda2/trunk/common/cdaContentItem.type.php $
 */

include_once("SecureInputCast.class.php");

/**
 * SecureInput
 * provides a library to enforce secure input parameters for applications. Replaces $_GET, $_POST, $_REQUEST and $_COOKIE
 *
 * @package SecureInput
 */
class SecureInput
{
    // Basic types
    const SI_TYPE_BOOL   = "bool";
    const SI_TYPE_INT    = "int";
    const SI_TYPE_FLOAT  = "float";
    const SI_TYPE_DATE   = "date";
    const SI_TYPE_STRING = "string";
    const SI_TYPE_ARRAY  = "array";

    // Secure String Types
    const SI_TYPE_XML      			= "string_xml";     			// Valid XML
    const SI_TYPE_PLAIN    			= "string_plain";   			// Only basic characters: A-z 0-9 .-_,&!?
    const SI_TYPE_PLAINEXT 			= "string_plainext";			// Same as plain but including any foreign letters
    const SI_TYPE_PLAINEXT_COMMENT 	= "string_plain_comment";		// Same as plainext but including any foreign letters and comment e. g. ?!
    const SI_TYPE_PLAINEXT_SEARCH 	= "string_plainext_search";		// Same as plainext but including any foreign letters and " ' (for phrase search)
    const SI_TYPE_ENC      			= "string_enc";     			// Encoded html entities
    const SI_TYPE_URL      			= "string_url";     			// complete URL
    const SI_TYPE_URLQ     			= "string_urlq";    			// complete URL with query parameters
    const SI_TYPE_URLL    			= "string_urll";    			// complete URL with login parameters
    const SI_TYPE_URLQL    			= "string_urlql";   			// complete URL with login and query parameters
    const SI_TYPE_EMAIL    			= "string_email";   			// email address

    const SI_SOURCE_GET      = 1;
    const SI_SOURCE_POST     = 2;
    const SI_SOURCE_COOKIE   = 4;
    const SI_SOURCE_REQUEST  = 3;
    const SI_SOURCE_ALL      = 7;

    // process sourcees for input parameters in this order ( This array includes all real sources, not groups of sources like SI_SOURCE_REQUEST or SI_SOURCE_ALL)
    static public $source_order = array(self::SI_SOURCE_COOKIE, self::SI_SOURCE_GET, self::SI_SOURCE_POST);

    static protected $store = null;

    /**
     * Construct a new instance and prepare input arrays
     *
     * @param bool $remove_source    if true (the default) it deletes $_GET, $_POST and $_REQUEST after storing it's contents to discourage usage of them
     * @param bool $force_reset      if true (default is false) SecureInput will get it's values from $_GET,$_POST,$_COOKIE again, regardless of it's current state. Usefull for unit tests with manipulated $_GET
     */
    public function __construct($remove_source = true, $force_reset = false)
    {
    	if(self::$store === null || $force_reset)
    	{
            self::$store[self::SI_SOURCE_GET] = $_GET;
            self::$store[self::SI_SOURCE_POST] = $_POST;
            self::$store[self::SI_SOURCE_COOKIE] = $_COOKIE;
    	}

        if($remove_source)
        {
            $_REQUEST = array();
            $_GET = array();
            $_POST = array();
            $_COOKIE = array();
        }

        //Handling XSS Attack through the Header Referrer Value
        if (isset($_SERVER['HTTP_REFERER']))
        {
        	$_SERVER['HTTP_REFERER'] = self::XSS_Clean($_SERVER['HTTP_REFERER']);
        }
    }


    /**
     * get a variable of specified type from inside the SecureInput class's store
     *
     * @param string $key      name of variable to load
     * @param string $type     type constant for variable (eg: SecureInput::SI_TYPE_STRING)
     * @param mixed $default   default value to use if variable is not present or invalid
     * @param bool $sanitize   if true (default) sanitize string, if false return null when validation fails
     * @param bool $strict     Also ensure the type of any passed variable while validating
     * @param string $source   Constant of data source to load variables from (Defaults to SecureInput::SI_SOURCE_REQUEST)
     * @param bool $quiet      suppress warning for unsafe get. ONLY USE IN UNITTESTs
     */
    public function get($key, $type, $default = null, $sanitize = true, $strict = false, $source = self::SI_SOURCE_REQUEST, $quiet = false)
    {
        $value = null;
        $raw = null;

        // Get raw value from store (if set)
        foreach(self::$source_order as $source_id)
            if(($source_id & $source) && isset(self::$store[$source_id][$key]))
                $raw = self::$store[$source_id][$key];

        // Cast to specified type (if not null)
        if(!is_null($raw))
            $value = $this->cast($raw, $type, $sanitize, $strict, $quiet);

        // Use default if no value found
        if(is_null($value) && !is_null($default))
            $value = $default;

        return $value;
    }


    /**
     * filter a variable of specified type from inside the SecureInput class's store
     *
     * @param string $value    the value to be sanitized
     * @param string $type     type constant for variable (eg: SecureInput::SI_TYPE_STRING)
     * @param mixed $default   default value to use if variable is not present or invalid
     * @param bool $sanitize   if true (default) sanitize string, if false return null when validation fails
     * @param bool $strict     Also ensure the type of any passed variable while validating
     * @param string $source   Constant of data source to load variables from (Defaults to SecureInput::SI_SOURCE_REQUEST)
     * @param bool $quiet      suppress warning for unsafe get. ONLY USE IN UNITTESTs
     */
    public function filter($value, $type, $default = null, $sanitize = true, $strict = false, $source = self::SI_SOURCE_REQUEST, $quiet = false)
    {
    	$returnValue = null;
    	// Cast to specified type (if not null)
    	if(!is_null($value)) {
    		$returnValue = $this->cast($value, $type, $sanitize, $strict, $quiet);
    	}

    	// Use default if no value found
    	if(is_null($returnValue) && !is_null($default)) {
    		$returnValue = $default;
    	}
    	return $returnValue;
    }


    /**
     * set a variable inside the internal store
     *
     * @param string $key       name of variable to store
     * @param mixed $value      value to store
     * @param string $source    Constant of data source to load variables from (Defaults to SecureInput::SI_SOURCE_GET
     */
    public function set($key, $value, $source = self::SI_SOURCE_GET)
    {
        if(!isset(self::$store[$source]))
            throw new Exception("SecureInput: Invalid source '$source' specified");

        self::$store[$source][$key] = $value;
    }


    /**
     * remove a variable from inside the internal store
     *
     * @param string $key      name of variable to store
     * @param string $source   Constant of data source to load variables from (Defaults to SecureInput::SI_SOURCE_GET
     */
    public function del($key, $source = self::SI_SOURCE_GET)
    {
        if(!isset(self::$store[$source]))
            throw new Exception("SecureInput: Invalid source '$source' specified");

        if(isset(self::$store[$source][$key]))
            unset(self::$store[$source][$key]);
    }


    /**
     * convert the specified store to a Query string. Should only be used with the SI_SOURCE_GET store
     */
    public function __toString()
    {
        // Get raw value from store (if set)
        $out = array();
            foreach(self::$store[self::SI_SOURCE_GET] as $key => $value)
                if (is_array($value))
                {
                    foreach($value as $subkey => $subvalue)
                        $out[] = $key ."[]=".urlencode(SecureInputCast::toString($subvalue));
                }
                else
                    $out[] = $key."=".urlencode(SecureInputCast::toString($value));

        return implode("&",$out);
    }


    public function getSecureQueryString($intKeys=array()) {
    	// Get raw value from store (if set)
        $out = array();
            foreach(self::$store[self::SI_SOURCE_GET] as $key => $value)
                if (is_array($value))
                {
                    foreach($value as $subkey => $subvalue)
                        $out[] = $key ."[]=".$this->cast($subvalue,self::SI_TYPE_PLAINEXT);
                }
                else {
                	if(in_array($key,$intKeys)) {
                		$out[] = $key."=".$this->cast($value,self::SI_TYPE_INT);

                	}
                	else {
                		$out[] = $key."=".$this->cast($value,self::SI_TYPE_PLAINEXT);

                	}
                }

        $out = implode("&",$out);
        if(!get_magic_quotes_gpc()) {
        	$out = addslashes($out);

        }
        // XSS Attack Removal in cae there were any '"' signs or '/' '//' signs since Firefox has a vulnureability with an unescaped '"' signs
        $out = str_replace('\\"',' ', $out);

        return $out;
    }


    ///////////////////////////////////////////////////////////////////////////

    /**
     * Check the type of a variable. A value of null allways returns false.
     *
     * @param mixed $value The value to validate.
     * @param string $type A type identifier, see {@link TypeValidator::$isType}.
     * @return bool true if the value has the correct type, false otherwise.
     */
    static public function checkType($value, $type)
    {
        if(is_null($value)) return false;

        switch($type)
        {
            case self::SI_TYPE_BOOL:    return SecureInputCast::isBool($value);
            case self::SI_TYPE_INT:     return SecureInputCast::isInt($value);
            case self::SI_TYPE_FLOAT:   return SecureInputCast::isFloat($value);
            case self::SI_TYPE_DATE:    return SecureInputCast::isDate($value);
            case self::SI_TYPE_STRING:  return SecureInputCast::isString($value);
            case self::SI_TYPE_ARRAY:   return SecureInputCast::isArray($value);
            case self::SI_TYPE_XML:     return SecureInputCast::isXML($value);
        }
    }


    /**
     * cast 4value to specified type and validate it
     *
     * @param string $value     The value to cast
     * @param string $type      target type of value
     * @param bool $sanitize    if true (default) try to enforce/cleanup value to fit into it's target type. If not return null if validation fails
     * @param bool $strict      also check type of value when validating it
     */
    static protected function cast($value, $type, $sanitize = true, $strict = false, $quiet = false)
    {
        $original = $value;
        $cast     = null;
        $valid    = false;

        //Warn about insecure types
        if(!$quiet && in_array($type, array(self::SI_TYPE_STRING, self::SI_TYPE_ARRAY)))
        {
        	$debug = debug_backtrace();
        	$debugRes = array();
        	foreach($debug as $call) {
        		$debugRes[] = array (
        				'file' => isset($call['file']) ? $call['file'] : '',
        				'line' => isset($call['line']) ? $call['line'] : '',
        				'function' => isset($call['function']) ? $call['function'] : '',
        				'class' => isset($call['class']) ? $call['class'] : '',
        		);
        	}
        	error_log("The type \"".$type."\" was used in: \n" . print_r($debugRes, true) . ".");
        	trigger_error("SecureInput: Use of type '$type' is not safe", E_USER_WARNING);
        }

        switch($type)
        {
            case self::SI_TYPE_BOOL:    			$cast  = SecureInputCast::toBool($value, $valid);              		break;
            case self::SI_TYPE_INT:     			$cast  = SecureInputCast::toInt($value, $valid);               		break;
            case self::SI_TYPE_FLOAT:   			$cast  = SecureInputCast::toFloat($value, $valid);             		break;
            case self::SI_TYPE_DATE:    			$cast  = SecureInputCast::toDate($value, $valid);              		break;
            case self::SI_TYPE_STRING:  			$cast  = SecureInputCast::toString($value, $valid);            		break;
            case self::SI_TYPE_XML:     			$cast  = SecureInputCast::toXML($value, $valid);               		break;
            case self::SI_TYPE_ARRAY:   			$cast  = SecureInputCast::toArray($value, $valid);             		break;
            case self::SI_TYPE_PLAIN:   			$cast  = SecureInputCast::toStringPlain($value, $valid);       		break;
            case self::SI_TYPE_PLAINEXT:			$cast  = SecureInputCast::toStringPlainExt($value, $valid);    		break;
            case self::SI_TYPE_PLAINEXT_COMMENT:	$cast  = SecureInputCast::toStringPlainExtComment($value, $valid);	break;
            case self::SI_TYPE_PLAINEXT_SEARCH:		$cast  = SecureInputCast::toStringPlainExtSearch($value, $valid);	break;
            case self::SI_TYPE_ENC:     			$cast  = SecureInputCast::toStringEnc($value, $valid);         		break;
            case self::SI_TYPE_EMAIL:   			$cast  = SecureInputCast::toStringEmail($value, $valid);       		break;
            case self::SI_TYPE_URL:
            case self::SI_TYPE_URLQ:
            case self::SI_TYPE_URLL:
            case self::SI_TYPE_URLQL:   			$cast  = SecureInputCast::toStringURL($value, $valid, $type);  break;

            default: throw new Exception ("SecureInput: Type '$type' is unknown");
        }

        if(!$sanitize && $strict)
            return ($valid && $cast === $original) ? $cast : null;

        if(!$sanitize)
            return ($valid && $cast == $original) ? $cast : null;

        return $cast;
    }


    /** Function to deal with an escaping of most of the common XSS attacks can deal with different combinations and
     * approaches but still cannot guaruantee a 100% coverness
     *
     * @param string $data     The value of the system parameter to be cleaned
     * Inspired but this doc: https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet
     * */
    public function XSS_Clean($data)
    {
    	// Fix &entity\n;
    	$data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
    	$data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
    	$data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
    	$data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

    	// Remove any attribute starting with "on" or xmlns
    	$data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

    	// Remove javascript: and vbscript: protocols
    	$data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
    	$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
    	$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

    	// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
    	$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    	$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    	$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

    	// Remove namespaced elements (we do not need them)
    	$data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

    	do
    	{
    		// Remove really unwanted tags
    		$old_data = $data;
    		$data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
    	}
    	while ($old_data !== $data);

    	// we are done...
    	return $data;
    }
}
