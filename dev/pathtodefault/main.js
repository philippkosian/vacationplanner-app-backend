$(document).ready (function() {

	var script = document.createElement('script');
	script.src = '../root/assets/jquery/dist/jquery.js';
	script.type = 'text/javascript';

	var configPath = "assets/js/config.json"; //path to the config.json file
	var groupsConfig;

	loadConfigFile(); //diese funktion ladet die config.json datei

	function loadConfigFile() { //standarduser page
	
		$.getJSON( configPath, function( json ) 
		{
			groupsConfig=json;
			initDropdownGroupOptions();
		});

	}

	//Groups options config array (standarduser page)
	function initDropdownGroupOptions() {

		var tempOptions = groupsConfig.Groups;

		for(var groupselectcontent in tempOptions) {
			
			var currentGroupOption=tempOptions[groupselectcontent];

			$("#groupselect").append($('<option></option>').attr("value",groupselectcontent ).text(currentGroupOption.dropdownText));
		}

		$("#groupselect").selectpicker('refresh');

	}

	//groups dropdown event listener (standarduser page)
	$("#groupselect").change(function() {

		var selectedText = $(this).find("option:selected").text();
					
		//console.log(selectedText);

		if (selectedText == 'All') {
			
			selectedText = '';

			$.ajax ({
    		method: "POST",
    		url: "../root/api.php?action=calendar",
    		data: {
    			selectedText:selectedText,
    		},
    		success: function (data) {
    			$('#calender_part').replaceWith(data);
    		}
    		});

		} else {
			
			$.ajax ({
    		method: "POST",
    		url: "../root/api.php?action=calendar",
    		data: {
    			selectedText:selectedText,
    		},
    		success: function (data) {
    			$('#calender_part').replaceWith(data);
    		}
    		});
		}
    	
  	});

	$('input[name="datefilter"]').daterangepicker({
		autoUpdateInput: false,
		opens: "center",
		locale: {
	    	cancelLabel: 'Clear'
		}
	}); 

	$('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('YYYY-MM-DD') + '  bis  ' + picker.endDate.format('YYYY-MM-DD'));

		if(('input[name="datefilter"]') !== '') {
			console.log(picker.startDate);
		} else {
			console.log("It did not work")
		}

	});

	// this gives the clear button it's clear abillity
	$('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
		$(this).val(null);
	});

});