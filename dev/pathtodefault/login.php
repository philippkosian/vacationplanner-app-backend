<?php

session_start();

require_once ('database-files/datenbank-verbindung.php');
require_once ('database-files/User.class.php');
require_once ('database-files/Urlaub.class.php');
require_once ('password_compat-1.0.4/lib/password.php');
include_once "SecureInput/SecureInput.class.php";

$SecureInput = new SecureInput(false);

$GET = $SecureInput->get(1, SecureInput::SI_SOURCE_GET);
$POST = $SecureInput->get(2, SecureInput::SI_SOURCE_POST);
$COOKIE = $SecureInput->get(4, SecureInput::SI_SOURCE_COOKIE);
$REQUEST = $SecureInput->get(3, SecureInput::SI_SOURCE_REQUEST);

$email = $SecureInput->get("email", SecureInput::SI_TYPE_EMAIL, NULL);
$password = $SecureInput->get("password", SecureInput::SI_TYPE_PLAINEXT, NULL);

if(count($_POST)>0) {

	$user = new User($db_link);
	
	$userData = $user->getUserByEmail($email);

	//Überprüfung des Passworts
	if($userData !== false && password_verify($password, $userData['password'])) {
		
		$_SESSION['userid'] = $userData['user_id'];

		$usertype = $userData['usertype'];

		$isUserActivated = $userData['activated'];

		print_r($isUserActivated);

		if ($isUserActivated == 1) {
			if ($usertype == 0) {
				header("Location: standarduser.php");
			}
			if ($usertype == 1) {
				header("Location: adminuser.php");
			} else {
				$errorMessage = '<div class="container">'.'<div class="row">'.'<div class="col-md-4">'.'<p class="registration-error-message">&nbsp- &nbsp Something went wrong</p>'.'</div>'.'</div>'.'</div>';
			}
		} else {
			$errorMessage = '<div class="container">'.'<div class="row">'.'<div class="col-md-4">'.'<p class="registration-error-message">&nbsp- &nbsp User is not activated yet</p>'.'</div>'.'</div>'.'</div>';
		}

	} else {
		$errorMessage = '<div class="container">'.'<div class="row">'.'<div class="col-md-4">'.'<p class="registration-error-message">&nbsp- &nbsp Something is wrong with the given email or password<br></p>'.'</div>'.'</div>'.'</div>';
	}
}

?>

<!DOCTYPE html>

	<head>

		<!-- Required Prerequisites -->
		<script type="text/javascript" src="usefulls/jquery/dist/jquery.min.js"></script>
		<script type="text/javascript" src="usefulls/moment/min/moment.min.js"></script>
		<link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css">

		<!-- Hover Effects -->
		<link rel="stylesheet" href="usefulls/hover-effects-master/css/hover.css">

		<!-- Custom CSS -->
	    <link rel="stylesheet" href="css/login_register.css">

	    <!-- font-awesome -->
	    <link rel="stylesheet" href="usefulls/font-awesome-4.6.3/css/font-awesome.css">

	    <!-- Bootstrap select -->
	    <script type="text/javascript" src="usefulls/bootstrap-select/dist/js/bootstrap-select.js"></script>
	    <link rel="stylesheet" href="usefulls/bootstrap-select/dist/css/bootstrap-select.css"/>

		<!-- Include Date Range Picker -->
		<script type="text/javascript" src="usefulls/bootstrap-daterangepicker-master/daterangepicker.js"></script>
		<link rel="stylesheet" href="usefulls/bootstrap-daterangepicker-master/daterangepicker.css"/>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>

		<!-- The Main JS -->
		<script type="text/javascript" src="js/main.js"></script>

		<title>Login</title>

	</head>

	<body>
		
		<div class="container">
					
			<form class="form-group-lg form-signin" action="" method="post" role="login">

				<div class="row margin-bottom-1">

					<div class="col-md-12 text-center">
				
						<h1 class="display-1">Login</h1>

					</div>
				
				</div>

				<div class="row">

					<div class="col-md-12">
						
						<label class="sr-only">email</label>

						<input type="email" name="email" class="form-control input-lg" placeholder="Email">

					</div>

				</div>

				<div class="row">
					
					<div class="col-md-12">
						
						<label class="sr-only">password</label>

						<input type="password" name="password" class="form-control input-lg" placeholder="Password">

					</div>

				</div>

				<div class="row">

					<div class="col-md-12">

						<span title="Login: Access type depends on given user">
							
							<button type="submit" value="Abschicken" class="btn btn-primary btn-lg btn-block hvr-shadow">

								Login
								<i class="fa fa-sign-in"></i>

							</button>

						</span>
						
					</div>

				</div>

				<div class="row">
					
					<div class="col-md-12">

						<div class="col-md-4 pull-left">
							
							<a href="registration.php" id="link-to-register" class="pull-left">No access yet? Click me to become a member!</a>

						</div>

						<div class="col-md-4 pull-right">

							<a href="request-password-reset.php" id="link-to-register" class="pull-right">Forgot your password?</a>
							
						</div>

					</div>

				</div>

				<div class="row">

					<div class="col-md-12">

						<?php 

							if (isset($errorMessage)) {

								echo $errorMessage;

							}

						?>
						
					</div>

				</div>

			</form>

		</div>

	</body>

</html>